FROM alpine

# Add inotify-wait which is used to watch for changes in a directory
# once a change has been detected `httpd` configuration is reloaded
RUN apk --no-cache add inotify-tools
